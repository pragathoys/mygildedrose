class GildedRose
  def initialize(items)
    @items = items
  end
	
	# Return the items
	def items
		@items
	end
	
	# Handle the decrease of the sell_in value			
	def update_sell_in(item)		
		if item.name != 'Sulfuras, Hand of Ragnaros'
			item.sell_in - 1
		else
			item.sell_in
		end
		
	end	

	# Handle the decrease of the quality value		
  def update_quality()
		quality_decrease_rate 	= 1
		
    @items.each do |item|
			
			#####-----------------------------------------------			
			item_quality_decrease_rate = quality_decrease_rate
			# If the sell date has already passed
			if item.sell_in <= 0
				item_quality_decrease_rate = 2 * quality_decrease_rate
			end
			
				#Aged Brie will improve
			if item.name == 'Aged Brie'
				item_quality_decrease_rate = -1 * item_quality_decrease_rate
				
				#'Sulfuras', being a legendary item
			elsif item.name == 'Sulfuras, Hand of Ragnaros'
				item_quality_decrease_rate = 0
				
				#'Backstage passes', like aged brie, increases in Quality based on days left
			elsif item.name == 'Backstage passes to a TAFKAL80ETC concert'
				if item.sell_in <= 0 
					item_quality_decrease_rate = item.quality # it loses its value instantly
				elsif item.sell_in <= 5 
					item_quality_decrease_rate = -3 * quality_decrease_rate
				elsif item.sell_in <= 10
					item_quality_decrease_rate = -2 * quality_decrease_rate
				else
					item_quality_decrease_rate = -1 * quality_decrease_rate					
				end				
				
				#'Conjured' items degrade in Quality twice 
			elsif item.name == 'Conjured Mana Cake'
				item_quality_decrease_rate = 2 * quality_decrease_rate
			end
			
			#check for the quality mark of 50 except the sulfurus
			if item.name != 'Sulfuras, Hand of Ragnaros' && (item.quality - item_quality_decrease_rate) > 50
				item.quality = 50
				item_quality_decrease_rate = 0
			end
			
			### APPLY HERE THE DECREAMENT FOR THE QUALITY ######
			item.quality = item.quality - item_quality_decrease_rate
			##--------------------------------------------------
			
			#####-----------------------------------------------
			### APPLY HERE THE DECREAMENT FOR THE SELL IN ######
			item.sell_in = update_sell_in(item)		
			

			
    end
  end
end

class Item
  attr_accessor :name, :sell_in, :quality

  def initialize(name, sell_in, quality)
    @name = name
    @sell_in = sell_in
    @quality = quality
  end

  def to_s()
    "#{@name}, #{@sell_in}, #{@quality}"
  end
end
