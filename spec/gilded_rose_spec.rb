require_relative "../gilded_rose"

RSpec.describe GildedRose do
	
	before :each do
		@items = [
			Item.new(name='+5 Dexterity Vest', sell_in=10, quality=20),
			Item.new(name='+5 Dexterity Vest', sell_in=2, quality=4),
			Item.new(name='Aged Brie', sell_in=2, quality=0),
			Item.new(name='Elixir of the Mongoose', sell_in=5, quality=7),
			Item.new(name='Sulfuras, Hand of Ragnaros', sell_in=0, quality=80),
			Item.new(name='Sulfuras, Hand of Ragnaros', sell_in=-1, quality=80),
			Item.new(name='Backstage passes to a TAFKAL80ETC concert', sell_in=15, quality=20),
			Item.new(name='Backstage passes to a TAFKAL80ETC concert', sell_in=10, quality=49),
			Item.new(name='Backstage passes to a TAFKAL80ETC concert', sell_in=5, quality=49),
			Item.new(name='Backstage passes to a TAFKAL80ETC concert', sell_in=2, quality=39),
			Item.new(name='Conjured Mana Cake', sell_in=3, quality=6)
		]
  
		@gilded_rose = GildedRose.new @items		
		
		@days = 3
		(0...@days).each do |day|
			@gilded_rose.update_quality
		end		
	end
	
	describe '#update_quality' do
		
		it "At the end of each day our system lowers the value of sell_in" do
			expect(@gilded_rose.items[0].sell_in).to eq 7
		end

		it "At the end of each day our system lowers the value of quality" do
			expect(@gilded_rose.items[0].quality).to eq 17
		end
		
		it "Once the sell by date has passed, Quality degrades twice as fast" do
			expect(@gilded_rose.items[1].quality).to eq 0
		end			

		it "The Quality of an item is never negative" do
			expect(@gilded_rose.items[1].quality).to be >=0 
		end	
		
		it "The Quality of an item is never more than 50" do
			expect(@gilded_rose.items[8].quality).to eq 50		
		end					
		
		it "'Aged Brie' actually increases in Quality the older it gets" do
			expect(@gilded_rose.items[2].quality).to eq 4
		end			
		
		it "'Sulfuras', being a legendary item, never has to be sold or decreases in Quality" do
			expect(@gilded_rose.items[4].sell_in).to eq 0
			expect(@gilded_rose.items[4].quality).to eq 80
		end		
		
		it "'Backstage passes', like aged brie, increases in Quality as its SellIn value approaches; Quality increases by 2 when there are 10 days or less and by 3 when there are 5 days or less but Quality drops to 0 after the concert" do
			expect(@gilded_rose.items[6].sell_in).to eq 12
			expect(@gilded_rose.items[6].quality).to eq 23

			expect(@gilded_rose.items[7].sell_in).to eq 7
			expect(@gilded_rose.items[7].quality).to eq 50			
			
			expect(@gilded_rose.items[8].sell_in).to eq 2
			expect(@gilded_rose.items[8].quality).to eq 50					

			expect(@gilded_rose.items[9].sell_in).to be <= 0
			expect(@gilded_rose.items[9].quality).to eq 0								
		end				
		
		it "Conjured' items degrade in Quality twice as fast as normal items" do
			expect(@gilded_rose.items[10].sell_in).to eq 0
			expect(@gilded_rose.items[10].quality).to eq 0
		end						
		
		
	end
	
end